const _ = require('lodash');

const addAccountType = (highscoresData) => {
    console.log("evaluting account type");
    let totalXp = [
        highscoresData.Standard.progressData.xp,
        highscoresData.Ironman && highscoresData.Ironman.progressData.xp || 0,
        highscoresData.Ultimate && highscoresData.Ultimate.progressData.xp || 0,
        highscoresData.Hardcore && highscoresData.Hardcore.progressData.xp || 0
    ];
    highscoresData.accountType = HIGHSCORE_TYPES[_.lastIndexOf(totalXp,_.max(totalXp))];
    return highscoresData;
}

const HIGHSCORES_STANDARD_ACCOUNT_URL = "http://services.runescape.com/m=hiscore_oldschool/index_lite.ws?player=";
const HIGHSCORES_IRONMAN_ACCOUNT_URL = "http://services.runescape.com/m=hiscore_oldschool_ironman/index_lite.ws?player=";
const HIGHSCORES_ULTIMATE_ACCOUNT_URL = "http://services.runescape.com/m=hiscore_oldschool_ultimate/index_lite.ws?player=";
const HIGHSCORES_HARDCORE_URL = "http://services.runescape.com/m=hiscore_oldschool_hardcore_ironman/index_lite.ws?player=";

const HIGHSCORE_TYPES = ['Standard', 'Ironman', 'Ultimate', 'Hardcore'];

import { transformHighscoresDataToPlayer } from '../../util/player';

let getHighscoresData = (userName, url) => {
    return fetch(url + userName)
        .then(function (response) {
            if (response.status !== 200) {
                throw new Error("Unable to get player data");
            }
            return response.text().then(function (text) {
                return transformHighscoresDataToPlayer(userName, text);
            });
        })
        .catch(function (err) {
            return err;
        })
}

let getPlayerData = (userName) => {
    return Promise.all([
        getHighscoresData(userName, HIGHSCORES_STANDARD_ACCOUNT_URL),
        getHighscoresData(userName, HIGHSCORES_IRONMAN_ACCOUNT_URL),
        getHighscoresData(userName, HIGHSCORES_ULTIMATE_ACCOUNT_URL),
        getHighscoresData(userName,HIGHSCORES_HARDCORE_URL)
    ]).then((highscoresData) => {
        if (highscoresData[0] instanceof Error) {
            //standard must exist or banned??
            return new Error("Failed to get player data");
        }
        return addAccountType (
            _.omitBy(
                _.zipObject(HIGHSCORE_TYPES, highscoresData),
                _.isError
            )
        );
    }).catch(err => {
        return new Error("Failed to get player data");
    })
}

module.exports = {
    getPlayerData
}