import { createStore, applyMiddleware, combineReducers } from 'redux';
import createLogger from 'redux-logger';
import thunk from 'redux-thunk';
import setupApp from './modules/SetupApp';
import homeApp from './modules/HomeApp';
import maxCalcApp from './modules/MaxCalcApp';
import skillCalcApp from './modules/SkillCalcApp';
import moneyMakingApp from './modules/MoneyMakingApp';
import efficiencyApp from './modules/EfficiencyApp';
const reducer = combineReducers({
  setupApp,
  homeApp,
  maxCalcApp,
  skillCalcApp,
  moneyMakingApp,
  efficiencyApp
});

const configureStore = (initialState) => createStore(
  reducer,
  initialState,
  applyMiddleware(thunk, createLogger)
);
export default configureStore;