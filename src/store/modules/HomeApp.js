import * as actions from '../../actionTypes';
import { getPlayerData } from '../highscores/highscores.service';
import { Actions, Scene, Router } from 'react-native-router-flux';
import { Linking } from 'react-native';

const initialState = {
    username: null,
    isFetching: false,
    error: null,
    user: null,
    page: null,
    url: null
}

export function navToPage(pageRouterName) {
    return (dispatch) => {
        dispatch(navigateToPage(pageRouterName));
        Actions[pageRouterName]();
    }
}

export function navToURL(url) {
    return (dispatch) => {
        dispatch(navigateToUrl(url));
        Linking.openURL(url).catch(err => console.error('An error occurred', err));
    }
}

export function navigateToPage(pageRouterName) {
    return {
        type: actions.PAGE_NAVIGATE,
        page: pageRouterName
    }
}

export function navigateToUrl(url) {
    return {
        type: actions.URL_NAVIGATE,
        url: url
    }
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case actions.SETUP_SUCCESS:
            return {
                ...state,
                user: action.user,
                isFetching: action.isFetching,
                username: action.username
            }
        default:
            return state;
    }
}