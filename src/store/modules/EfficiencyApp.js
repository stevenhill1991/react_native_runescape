import * as actions from '../../actionTypes';

const initialState = {
    selectedSkill: null
}

export function selectSkill(skillName) {
    return {
        type: actions.EFFICIENCY_SELECT_SKILL,
        selectedSkill: skillName
    }
}
export default function reducer(state = initialState, action) {
    switch (action.type) {
        case actions.EFFICIENCY_SELECT_SKILL:
            return {
                ...state,
                selectedSkill: action.selectedSkill
            }
        default:
            return state;
    }
}