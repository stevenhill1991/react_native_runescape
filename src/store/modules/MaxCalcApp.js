import * as actions from '../../actionTypes';
import { getPlayerData } from '../highscores/highscores.service';
import { Actions, Scene, Router } from 'react-native-router-flux';

const initialState = {
    username: null,
    isFetching: false,
    error: null,
    user: null,
    isSearching: false,
    isSelectingSkill: false,
    selectedSkill: null
}

export function searchUser(username, skipSetup) {
    return (dispatch) => {
        dispatch(searchUserRequest(username));
        getPlayerData(username).then((user) => {
            if (user) {
                dispatch(searchUserSuccess(user, username));
            }
        });
    }

}

export function searchUserRequest(username) {
    return {
        type: actions.MAXCALC_USER_SEARCH_REQUEST,
        isFetching: true,
        username
    }
}

export function searchUserSuccess(user, username) {
    return {
        type: actions.MAXCALC_USER_SEARCH_SUCCESS,
        user,
        isFetching: false,
        username,
        isSearching: false,
        isSelectingSkill: true,
        selectedSkill: null
    }
}

export function inputChange(username) {
    return {
        type: actions.MAXCALC_INPUT_CHANGED,
        username
    }
}

export function startSearchUser() {
    return {
        type: actions.MAXCALC_START_SEARCH_USER,
        isSearching: true
    }
}

export function skillSelected(skillName) {
    return {
        type: actions.MAXCALC_SKILL_SELECTED,
        selectedSkill: skillName,
        isSelectingSkill: false
    }
}

export function skillCanceled() {
    return {
        type: actions.MAXCALC_SKILL_CANCELED,
        isSelectingSkill: false
    }
}

export function selectSkill() {
    return {
        type: actions.MAXCALC_SELECT_SKILL,
        isSelectingSkill: true
    }
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case actions.PAGE_NAVIGATE:
            if (action.page === "maxcalc") {
                return {
                    ...state,
                    username: action.username,
                    isSearching: action.username == null
                }
            }
            return state;
        case actions.MAXCALC_SKILL_SELECTED:
            return {
                ...state,
                selectedSkill: action.selectedSkill,
                isSelectingSkill: action.isSelectingSkill
            }
        case actions.MAXCALC_SKILL_CANCELED:
            return {
                ...state,
                isSelectingSkill: action.isSelectingSkill
            }
        case actions.MAXCALC_INPUT_CHANGED:
            return {
                ...state,
                username: action.username
            }

        case actions.MAXCALC_USER_SEARCH_REQUEST:
            return {
                ...state,
                username: action.username,
                isFetching: action.isFetching
            }
        case actions.MAXCALC_USER_SEARCH_SUCCESS:
            return {
                ...state,
                username: action.username,
                isFetching: action.isFetching,
                user: action.user,
                isSearching: action.isSearching,
                isSelectingSkill: action.isSelectingSkill,
                selectedSkill: action.selectedSkill
            }

        case actions.MAXCALC_START_SEARCH_USER:
            return {
                ...state,
                isSearching: action.isSearching
            }
        case actions.MAXCALC_SELECT_SKILL:
            return {
                ...state,
                isSelectingSkill: action.isSelectingSkill
            }
        default:
            return state;
    }
}