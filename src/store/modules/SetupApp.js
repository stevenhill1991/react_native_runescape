import * as actions from '../../actionTypes';
import { getPlayerData } from '../highscores/highscores.service';
import { Actions, Scene, Router } from 'react-native-router-flux';

// general flow

// search user -> found user -> (setupSubmit -> homeApp (user) || closePlayerFoundModal -> (repeat) -> setupSubmit(skipSetup) -> homeApp (no user))
export function searchUser(username) {
    return (dispatch) => {
        dispatch(setupRequest(username));

        if (!username) {
            return dispatch(setupError("Invalid username"));
        }

        getPlayerData(username).then((user) => {
            if (user instanceof Error) {
                return disatch(setupError(user.message));
            }
            if (user) {
                return dispatch(foundUser(user, username));
            }
        });
    }
}

export function setupError(errorMessage) {
    return {
        type: actions.SETUP_ERROR,
        errorMessage,
        isFetching: false
    }
}

export function foundUser(user, username) {
    return {
        type: actions.SETUP_FOUND_USER,
        user,
        isFetching: false,
        username,
        showPlayerFoundModal: true
    }
}

export function closePlayerFoundModal() {
    return {
        type: actions.SETUP_CLOSE_PLAYER_FOUND_MODAL,
        showPlayerFoundModal: false
    }
}

export function setupSubmit(username, user, skipSetup) {
    return (dispatch) => {
        if (skipSetup) {
            Actions.replace("home");
            dispatch(setupSuccess(null, null));
        } else {
            Actions.replace("home");
            dispatch(setupSuccess(user, username));
        }

    }
}

export function setupRequest(username) {
    return {
        type: actions.SETUP_REQUEST,
        username: username,
        isFetching: true,
        errorMessage: null
    }
}

export function inputChange(username) {
    return {
        type: actions.SETUP_INPUT_CHANGED,
        username
    }
}

export function setupSuccess(user, username) {
    return {
        type: actions.SETUP_SUCCESS,
        user,
        isFetching: false,
        username,
        showPlayerFoundModal: true
    }
}

const initialState = {
    username: null,
    isFetching: false,
    error: null,
    user: null,
    showPlayerFoundModal: false,
    errorMessage: null
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case actions.PAGE_NAVIGATE:
            if (action.page === "setup") {
                return state;
            }
        case actions.SETUP_ERROR:
            return {
                ...state,
                errorMessage: action.errorMessage,
                isFetching: action.isFetching
            }
        case actions.SETUP_REQUEST:
            return {
                ...state,
                username: action.username,
                isFetching: action.isFetching,
                errorMessage: null
            }
        case actions.SETUP_SUCCESS:
            return {
                ...state,
                user: action.user,
                isFetching: action.isFetching,
                username: action.username,
                showPlayerFoundModal: action.showPlayerFoundModal
            }
        case actions.SETUP_FAILURE:
            return {
                ...state,
                isFetching: action.isFetching
            }
        case actions.SETUP_INPUT_CHANGED:
            return {
                ...state,
                username: action.username
            }
        case actions.SETUP_CLOSE_PLAYER_FOUND_MODAL:
            return {
                ...state,
                showPlayerFoundModal: action.showPlayerFoundModal
            }

        case actions.SETUP_FOUND_USER:
            return {
                ...state,
                user: action.user,
                isFetching: false,
                username: action.username,
                showPlayerFoundModal: true
            }
        default:
            return state;
    }
}