import * as actions from '../../actionTypes';

const initialState = {
    selectedGuide: null,
    selectedCategory: null,
}

export function selectGuide(guideName) {
    return {
        type: actions.MONEYMAKING_SELECT_GUIDE,
        selectedGuide: guideName
    }
}

export function selectCategory(categoryName) {
    return {
        type: actions.MONEYMAKING_SELECT_CATEGORY,
        selectedCategory: categoryName
    }
}

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case actions.PAGE_NAVIGATE:
            if (action.page === "moneymaking") {
                return state;
            }
        case actions.MONEYMAKING_SELECT_CATEGORY:
            return {
                ...state,
                selectedCategory: action.selectedCategory
            }
        case actions.MONEYMAKING_SELECT_GUIDE:
            return {
                ...state,
                selectedGuide: action.selectedGuide
            }
        default:
            return state;
    }
}