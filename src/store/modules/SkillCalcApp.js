import * as actions from '../../actionTypes';

const initialState = {
    username: null,
    user: null,
    isFetching: false,
    error: null,
    selectedSkill: null,
    selectedCategory: null,
}

export function selectSkill(skillName) {
    return {
        type: actions.SKILLCALC_SELECT_SKILL,
        selectedSkill: skillName
    }
}

export function selectCategory(catName) {
    return {
        type: actions.SKILLCALC_SELECT_CATEGORY,
        selectedCategory: catName
    }
}

export default function reducer(state = initialState, action) {

    switch (action.type) {
        case actions.PAGE_NAVIGATE:
            if (action.page === "skillcalc") {
                return {
                    state: initialState
                }
            }
        case actions.SETUP_SUCCESS:
            return {
                ...state,
                user: action.user,
                isFetching: action.isFetching,
                username: action.username
            }
        case actions.SKILLCALC_SELECT_SKILL:
            return {
                ...state,
                selectedSkill: action.selectedSkill
            }
        case actions.SKILLCALC_SELECT_CATEGORY:
            return {
                ...state,
                selectedCategory: action.selectedCategory
            }
        default:
            return state;
    }
}