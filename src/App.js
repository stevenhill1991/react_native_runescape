import React, { Component } from 'react';
import {
  View
} from 'react-native';
import { Provider, connect } from 'react-redux';
import configureStore from './store/configureStore';
const store = configureStore();

import { Actions, Scene, Router } from 'react-native-router-flux';
const ConnectedRouter = connect()(Router);

import SetupContainer from './containers/SetupContainer';
import HomeContainer from './containers/HomeContainer';
import MaxCalcContainer from './containers/MaxCalcContainer';
import SkillCalcContainer from './containers/SkillCalcContainer';
import MoneyMakingContainer from './containers/MoneyMakingContainer';
import EfficiencyContainer from './containers/EfficiencyContainer';

const Scenes = Actions.create(
  <Scene key="root">
    <Scene key="setup" component={SetupContainer} title="setup" hideNavBar></Scene>
    <Scene key="home" component={HomeContainer} title="home" hideNavBar></Scene>
    <Scene key="maxcalc" component={MaxCalcContainer} title="Max Calculator"></Scene>
    <Scene key="skillcalc" component={SkillCalcContainer} title="Skill Calculator"></Scene>
    <Scene key="moneymaking" component={MoneyMakingContainer} title="Money Making"></Scene>
    <Scene key="efficiency" component={EfficiencyContainer} title="Efficiency"></Scene>
  </Scene>
);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter scenes={Scenes} />
      </Provider>
    );
  }
}

export default App;