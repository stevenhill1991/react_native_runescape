//template to copy will be removed
import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet,
    TouchableOpacity
} from 'react-native';
import _ from 'lodash';

getGuideRow = (guide, guideKey, viewKey, onSelectGuide) => {
    return (
        <TouchableOpacity key={viewKey} onPress={onSelectGuide.bind(onSelectGuide, guideKey)}>
            <Text>{guide.title} - {guide.gpPerHour}</Text>
        </TouchableOpacity>
    )
}

const GuideList = ({guides, onSelectGuide}) => {
    let guideKeys = Object.keys(guides);
    console.log("GUIDEKEYSSSS", guideKeys);
    return (
        <View>
            {_.map(guides, (guide, key) =>  getGuideRow(guide, key,  key, onSelectGuide))}
        </View>
    );
}

const styles = StyleSheet.create({

});


export default GuideList;