//template to copy will be removed
import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet
} from 'react-native';
import ButtonComponent from '../../General/Button';
import LabelComponent from '../../General/Label';
import ErrorMessage from '../../General/ErrorMessage';

import Guides from './Guides';

import CategoryPicker from './CategoryPicker';
import GuideList from './GuideList';
import Guide from './Guide';

const GuidePicker = ({onSelectCategory, onSelectGuide}) => {


    return (
        <View style={styles.guidePicker}>
            <CategoryPicker 
                categories={["All"].concat(Object.keys(Guides))}
                onSelectCategory={onSelectCategory}
            />
            <GuideList 
                guides={Guides[
                    "Combat"//selectedCategory
                ]}
                onSelectGuide={onSelectGuide}
            />
            <Guide
                guideData={Guides["Combat"]["Green Dragons"]}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    guidePicker: {
        marginLeft: 20,
        marginRight: 20,
        alignItems: 'center',
    }
});


export default GuidePicker;