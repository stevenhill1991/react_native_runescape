//template to copy will be removed
import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import _ from 'lodash';
var width = Dimensions.get('window').width;
getCategoryRow = (category, key, onSelectCategory) => {
    return (
        <TouchableOpacity key={key} onPress={onSelectCategory.bind(onSelectCategory, category)}>
            <View style={styles.categoryContainer}>
                <Text style={styles.categoryText}>{category}</Text>
            </View>
        </TouchableOpacity>
    );
}
let categoriesLength;
const CategoryPicker = ({categories, onSelectCategory}) => {
    categoriesLength = categories.length;
    return (
        <View style={styles.categoriesContainer}>
            {_.map(categories, (category, key) => getCategoryRow(category, key, onSelectCategory))}
        </View>
    );
}

const styles = StyleSheet.create({
    categoriesContainer: {
        marginLeft: 20,
        marginRight: 20,
        alignItems: 'center',
        flexDirection: 'row',
    },
    categoryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: (width) /categoriesLength,
        backgroundColor: 'grey'
    },
    categoryText: {
        fontSize: 18,
        paddingLeft: 5
    },
});


export default CategoryPicker;