//template to copy will be removed
import React from "react";
import {
  View,
  Text,
  Button,
  TextInput,
  StyleSheet,
  TouchableOpactiy
} from "react-native";
import ButtonComponent from "../General/Button";
import LabelComponent from "../General/Label";
import ErrorMessage from "../General/ErrorMessage";
import { getXpForPlayerSkill, getLvlForPlayerSkill } from "../../util/player";
import { getExperienceForLevel } from "../../util/actions";
import { Grid, Row, Col } from "react-native-easy-grid";

class XPGoal extends React.Component {

  render () {
    const  {selectedSkill, selectedCategory, userData} = this.props;

    if (!selectedSkill || !selectedCategory) {
      return null;
    }
    let fromXp = getXpForPlayerSkill(selectedSkill, userData) || 0;
    let goalLevel = getLvlForPlayerSkill(selectedSkill, userData) + 1 || 2;
    let goalXp = getExperienceForLevel(goalLevel);

    return (
      <Grid style={styles.container}>
        <Row style={{ height: 30 }}>
          <Col>
            <Text style={styles.label}>Current Level</Text>
          </Col>
          <Col>
            <Text style={styles.label}>Current Experience</Text>
          </Col>
        </Row>
        <Row>
          <Col>
            <TextInput style={styles.textInput} placeholder="0" />
          </Col>
          <Col>
            <TextInput style={styles.textInput} placeholder="0" />
          </Col>
        </Row>
        <Row style={{ height: 30 }}>
          <Col>
            <Text style={styles.label}>Target Level</Text>
          </Col>
          <Col>
            <Text style={styles.label}>Target Experience</Text>
          </Col>
        </Row>
        <Row>
          <Col>
            <TextInput style={styles.textInput} placeholder="0" />
          </Col>
          <Col>
            <TextInput style={styles.textInput} placeholder="0" />
          </Col>
        </Row>
      </Grid>
    );
  }
};

const styles = StyleSheet.create({
  container: {
  
    backgroundColor: "#282828",
    margin: 20,
    padding: 0,
    flex: 0.5
  },
  label: {
    color: "white",
    margin: 10
  },
  textInput: {
    height: 40,
    color: "black",
    borderColor: "white",
    backgroundColor: "white",
    margin: 10
  }
});

export default XPGoal;
