import Prayer from './Prayer';
import Magic from './magic';
import Woodcutting from './woodcutting';
import Fletching from './fletching';
import Fishing from './fishing';
import Firemaking from './firemaking';
import Crafting from './crafting';
import Smithing from './smithing';
import Mining from './mining';
import Herblore from './herblore';
import Agility from './agility';
import Thieving from './thieving';
import Farming from './farming';
import Runecraft from './runecraft';
import Hunter from './hunter';
import Construction from './construction';
import Cooking from './cooking';

const methods = {
    ...Prayer,
    ...Magic,
    ...Woodcutting,
    ...Fletching,
    ...Fishing,
    ...Firemaking,
    ...Crafting,
    ...Smithing,
    ...Mining,
    ...Herblore,
    ...Agility,
    ...Thieving,
    ...Farming,
    ...Runecraft,
    ...Hunter,
    ...Construction,
    ...Cooking,
}

export default methods;