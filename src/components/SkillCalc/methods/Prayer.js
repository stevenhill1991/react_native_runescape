import _ from 'lodash';
const prayer = {
    "Bone": [
        {
            name: "Bones",
            xp: 5,
            req: ["Prayer", 1]
        },
        {
            name: "Big bones",
            xp: 15,
            req: ["Prayer", 1]
        },
        {
            name: "Dragon Bones",
            xp: 72,
            req: ["Prayer", 1]
        },
        {
            name: "Lava dragon bones",
            xp: 85,
            req: ["Prayer", 1]
        },
        {
            name: "Dagganoth bones",
            xp: 125,
            req: ["Prayer", 1]
        }
    ],
    "Ensouled": [
        {
            name: "Goblin", 
            xp: 130,
            req:["Prayer", 1, "Magic", 3]
        },
        {
            name: "Monkey", 
            xp: 182,
            req:["Prayer", 1, "Magic", 7]
        },
        {
            name: "Imp", 
            xp: 286,
            req:["Prayer", 1, "Magic", 12]
        },
        {
            name: "Minotaur", 
            xp: 364,
            req:["Prayer", 1, "Magic", 16]
        },
        {
            name: "Scorpion", 
            xp: 454,
            req:["Prayer", 1, "Magic", 19]
        }
    ]
}

const prayerModifiers = {
    "Gilded Altar": {
        boost: 3.5
    },
    "Necromancy": {
        boost: 1
    },
    "Ectofuntus": {
        boost: 4
    },
    "Bury": {
        boost: 1
    }
}

getBoneWithBoost = (bone, boost) => {
    bone.xp = bone.xp * prayerModifiers[boost].boost;
    return bone;
}

const actions = {
    Prayer: {
        "type": "table",
        "keys": ["Popular Methods", "Gilded Altar", "Necromancy", "Ectofuntus", "Bury"],
        "Popular Methods": [
            {
                name: "Dragon Bones (Gilded Altar)",
                xp: 252,
                req: ["Prayer", 1]
            },
            {
                name: "Dragon Bones (Ectofuntus)",
                xp: 288,
                req: ["Prayer", 1]
            },
            {
                name: "Ensouled Dragon Head",
                xp: 1560,
                req: ["Prayer", 1, "Magic", 93]
            }
        ],
        "Bury": _.map(prayer.Bone, (bone) => getBoneWithBoost(bone, "Bury")),
        "Gilded Altar": _.map(prayer.Bone, (bone) => getBoneWithBoost(bone, "Gilded Altar")),
        "Ectofuntus": _.map(prayer.Bone, (bone) => getBoneWithBoost(bone, "Ectofuntus")),
        "Necromancy": prayer.Ensouled
    }
};

export default actions; 

