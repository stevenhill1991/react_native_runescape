class CombatCalculator {

    constructor (playerStats) {
        this.setPlayerStats(playerStats);
    }

    setPlayerStats(playerStats) {
        this.hitpoints = playerStats.hitpoints;
        this.strength = playerStats.strength;
        this.attack = playerStats.attack;
        this.defence = playerStats.defence;
        this.magic = playerStats.magic;
        this.ranged = playerStats.ranged;
        this.prayer = playerStats.prayer;
    }

    getCombatLevelWithDetails() {
        var baseCombatLevel = (this.hitpoints + this.defence + Math.floor(this.prayer * 0.5)) * 0.25;
        var meleeCombatLevels = (this.strength + this.attack) * 0.325;
        var rangedCombatLevels = (Math.floor(this.ranged * 1.5)) * 0.325;
        var magicCombatLevels = (Math.floor(this.magic * 1.5)) * 0.325;
        var combatLevel = Math.floor(baseCombatLevel + Math.max(meleeCombatLevels, rangedCombatLevels, magicCombatLevels));
    
        return {
            combatLevel,
            ...this.getLevelsRequiredForNextCombatLevel(
                baseCombatLevel,
                ++combatLevel,
                meleeCombatLevels,
                rangedCombatLevels,
                magicCombatLevels,
                combatLevel
            )
        }
    }

    getLevelsRequiredForNextCombatLevel(baseCombatLevel,nextCombatLevel, meleeCombatLevels, rangedCombatLevels, magicCombatLevels) {
        var max = Math.max(meleeCombatLevels, rangedCombatLevels, magicCombatLevels);
        return {
            "Attack or Strength": this.getLevelsRequired((baseCombatLevel + meleeCombatLevels), nextCombatLevel, 0.325),
            "Hitpoints or Defence": this.getLevelsRequired((baseCombatLevel + max), nextCombatLevel, 0.25),
            "Magic": this.getLevelsRequired((baseCombatLevel + magicCombatLevels), nextCombatLevel, 0.325),
            "Ranged": this.getLevelsRequired((baseCombatLevel + rangedCombatLevels), nextCombatLevel, 0.25),
            "Prayer": this.getLevelsRequired((baseCombatLevel + max), nextCombatLevel, 0.125),
        }
    }

    getLevelsRequired (curr, goal, multiplier) {
        var levels = 0;
        while (curr < goal) {
            curr += multiplier;
            levels++;
        }
        return levels;
    }
    
    
}