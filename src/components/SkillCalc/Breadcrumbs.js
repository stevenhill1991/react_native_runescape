import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native';
import ButtonComponent from '../General/Button';
import LabelComponent from '../General/Label';
import ErrorMessage from '../General/ErrorMessage';

import _ from 'lodash';
var width = Dimensions.get('window').width;
import { Grid, Row, Col } from "react-native-easy-grid";
const Breadcrumbs = ({onSelectHome, onSelectSkill, onSelectCategory, isSkillSelected, isCategorySelected}) => {
    if (!isSkillSelected) {
        return null;
    }
    return (    
        <View style={styles.selectSkillRow}>
            <TouchableOpacity onPress={onSelectHome.bind(onSelectHome)}>
                <Text>Home </Text>
            </TouchableOpacity>
            {isCategorySelected ?   
            <TouchableOpacity onPress={onSelectSkill.bind(onSelectSkill, isSkillSelected)}>
                <Text>
                > {isSkillSelected} 
                </Text>
            </TouchableOpacity>
                : null
            }
        </View>
    );
}

const styles = StyleSheet.create({
    calcSelectorContainer: {
        marginLeft: 20,
        marginRight: 20
    },
    skillImage: {
        height: 20,
        width: 20,
    },
    selectSkillRow: {
        flexDirection: 'row',
        alignItems: 'center',
        borderWidth: 1,
        marginLeft: 20,
        marginRight: 20
    },
    selectSkillText: {
        fontSize: 18,
        paddingLeft: 5
    },
    skillRowsContainer: {
        marginLeft: 20,
        marginRight: 20
    },
    selectASkillContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
    }
});


export default Breadcrumbs;