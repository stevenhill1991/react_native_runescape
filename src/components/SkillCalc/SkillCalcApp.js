import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    Image,
    ProgressBar,
    ActivityIndicator,
    BackHandler,
} from 'react-native';
import LabelComponent from '../General/Label';
import CalcSelector from './CalcSelector';
import { Actions } from 'react-native-router-flux';  
import Categories from './Categories';
import ActionsComponent from './Actions';
import XPGoal from './XPGoal';
import Breadcrumbs from './Breadcrumbs';
import { getXpForPlayerSkill, getLvlForPlayerSkill } from '../../util/player';
import { getExperienceForLevel } from '../../util/actions';

class SkillCalcApp extends Component {
    constructor(props) {
        super();
        this._onBackPress = this.onBackPress.bind(this);
    }

    onBackPress() {
        BackHandler.remove
        BackHandler.removeEventListener('hardwareBackPress', this._onBackPress);
        return Actions.pop();
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this._onBackPress);
    }

    onSelectHomeBreadcrumb = () => {
        this.props.selectSkill(null);
        this.props.selectCategory(null);
    }

    onSelectSkillBreadcrumb = (skill) => {
        this.props.selectCategory(null);
        this.onSkillSelected(skill);
    }

    onSkillSelected = (skill) => {
        this.props.selectSkill(skill);
        this.fromXp = getXpForPlayerSkill(skill, this.props.skillCalcApp.user)
        this.goalLevel = getLvlForPlayerSkill(skill, this.props.skillCalcApp.user) + 1;
        this.goalXp = getExperienceForLevel(this.goalLevel);
        this.xpToGoal = this.goalXp - this.fromXp;
    }

    onSelectCategory = (category) => {
        this.props.selectCategory(category);
    }

    render() {
    
        return (
            <View style={{flex: 1}}>
                <XPGoal
                    selectedSkill={this.props.skillCalcApp.selectedSkill}
                    selectedCategory={this.props.skillCalcApp.selectedCategory}
                    userData={this.props.skillCalcApp.user}
                />
                <Breadcrumbs 
                    onSelectHome={this.onSelectHomeBreadcrumb}
                    onSelectSkill={this.onSelectSkillBreadcrumb}
                  isSkillSelected={this.props.skillCalcApp.selectedSkill}
                  isCategorySelected={this.props.skillCalcApp.selectedCategory}
                />
                <CalcSelector
                    onSkillSelected={this.onSkillSelected}
                    isSkillSelected={this.props.skillCalcApp.selectedSkill}
                    isCategorySelected={this.props.skillCalcApp.selectedCategory}
                    userData={this.props.skillCalcApp.user}
                />
                {this.props.skillCalcApp.selectedSkill && !this.props.skillCalcApp.selectedCategory ?
                    <Categories
                        selectedSkill={this.props.skillCalcApp.selectedSkill}
                        onSelectCategory={this.onSelectCategory}
                    />
                    :
                    null
                }

                {this.props.skillCalcApp.selectedSkill && this.props.skillCalcApp.selectedCategory ?
                    <ActionsComponent
                        selectedSkill={this.props.skillCalcApp.selectedSkill}
                        selectedCategory={this.props.skillCalcApp.selectedCategory}
                        xpToGoal={this.xpToGoal}
                    />
                    :
                    null
                }


            </View>
        );
    }
}
export default SkillCalcApp;

const styles = StyleSheet.create({

});