import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native';
import ButtonComponent from '../General/Button';
import LabelComponent from '../General/Label';
import ErrorMessage from '../General/ErrorMessage';

const skillCalculatorSkills = [
    'Prayer',
    'Magic',
    'Woodcutting',
    'Fletching',
    'Fishing',
    'Firemaking',
    'Crafting',
    'Smithing',
    'Mining',
    'Herblore',
    'Agility',
    'Thieving',
    'Farming',
    'Runecraft',
    'Hunter',
    'Construction',
    'Cooking'
]
import _ from 'lodash';

import SkillSelectorModal from '../General/SkillSelector/SkillSelectorModal';

const CalcSelector = ({ onSkillSelected, isSkillSelected, isCategorySelected, userData }) => {
    if (!onSkillSelected) {
        return null;
    }
    return (
     <View>
            <SkillSelectorModal
                onSubmit={onSkillSelected}
                onCancel={onSkillSelected}
                isVisible={!isSkillSelected}
                userData={userData}
                selectedSkill={isSkillSelected}
                enabledSkills={skillCalculatorSkills}
            />
        </View>
    );
}
const styles = StyleSheet.create({

});
export default CalcSelector;