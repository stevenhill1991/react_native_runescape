import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native';
import ButtonComponent from '../General/Button';
import LabelComponent from '../General/Label';
import ErrorMessage from '../General/ErrorMessage';
import methods from './methods/index';
import _ from 'lodash';
import { Col, Row, Grid } from "react-native-easy-grid";
var { height, width } = Dimensions.get('window');

getRequirements = (action, selectedSkill) => {
    if (action.req) {
        return (<Text style={styles.text}>{action.req[0]} {action.req[1]} </Text>);
    }
    if (action.level) {
        return (<Text style={styles.text}>Lvl. {action.level} </Text>);
    }
}

getAction = (action, key, xpToGoal, selectedSkill) => {
    return (
        <TouchableOpacity key={key} style={styles.actionContainer}>
            <Row>
                <Col size={0.20}>
                <Image source={{ uri: selectedSkill.toLowerCase() }} style={[styles.image, styles.center]}/>
                </Col>
                <Col size={0.80}>
                <Row>
                    <Text style={[styles.text, styles.bold]}>{action.name}</Text>
                </Row>
                <Row>
                    {getRequirements(action, selectedSkill)}
                    <Text style={styles.text}>({action.xp} exp) </Text>
                 
                    <Text style={styles.text}>{Math.ceil(parseFloat(xpToGoal) / action.xp) || 0} - actions</Text>
                </Row>
                </Col>
            </Row>
        </TouchableOpacity>
    )
}

const Actions = ({ selectedSkill, selectedCategory, xpToGoal }) => {
    if (!selectedSkill || !methods[selectedSkill] || !selectedCategory || !xpToGoal && xpToGoal != 0) {
        return null;
    }

    var skillContent = _.map(methods[selectedSkill][selectedCategory], (action, key) => {     
        return getAction(action, key, xpToGoal, selectedSkill)  
    })


    return (
        <ScrollView>
            {skillContent}
       </ScrollView>
    )
}

const styles = StyleSheet.create({
    actionsContainer: {
height: '50%'

    },
    actionContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: 'white',
        borderWidth: 1,
        padding: 2,
        backgroundColor: "#282828",
    },
    text: {
        color: 'white'
    },
    image: {
        height: 40,
        width: 40
    },
    center : {
        alignItems: 'center',
    },
    bold: {
        fontWeight: 'bold',
        fontSize: 16
    }
});


export default Actions;