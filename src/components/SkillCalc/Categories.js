import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet,
    Image,
    TouchableOpacity,
    ScrollView,
    Dimensions
} from 'react-native';
import ButtonComponent from '../General/Button';
import LabelComponent from '../General/Label';
import ErrorMessage from '../General/ErrorMessage';
import methods from './methods/index';
import _ from 'lodash';
var width = Dimensions.get('window').width;

getCategory = (onSelectCategory, catName, key) => {
    return (
        <TouchableOpacity key={key} onPress={onSelectCategory.bind(onSelectCategory, catName)}>
            <View style={styles.categoryContainer}>
                <Text style={styles.categoryText}>{catName}</Text>
            </View>
        </TouchableOpacity>
    )
}

const Categories = ({ selectedSkill, onSelectCategory }) => {
    if (!selectedSkill || !methods[selectedSkill]) {
        return null;
    }
    return (
        <View style={styles.categoriesContainer}>
            {_.map(methods[selectedSkill].keys, (catName, key) => getCategory(onSelectCategory, catName, key))}
        </View>
    )
}

const styles = StyleSheet.create({
    categoriesContainer: {
        marginLeft: 20,
        marginRight: 20,
        alignItems: 'center',
        flexDirection: 'column',
    },
    categoryContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        borderColor: 'black',
        borderWidth: 1,
        padding: 10,
        width: width * 0.8,
        backgroundColor: 'grey'
    },
    categoryText: {
        fontSize: 18,
        paddingLeft: 5
    },
});


export default Categories;