import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    Image,
    ProgressBar,
    ActivityIndicator,
    Linking,
    TouchableOpacity,
    ListView,
    BackHandler
} from 'react-native'

import HomeGrid from './HomeGrid';

import { homeImages, getGridData } from './HomeGridHelper';

import { Actions } from 'react-native-router-flux';
class HomeApp extends Component {
    
    onBackPress() {
        BackHandler.exitApp();
        return false;
    }

    constructor(props) {
        super();
        this._onBackPress = this.onBackPress.bind(this);
    }
    openPage = (title) => {
        this.props.navToPage(title, this.props.homeApp.username, this.props.homeApp.userData);
    }

    openUrl = (url) => {
        this.props.navToURL(url);
    }

    componentDidMount() {
        if (this.props.homeApp.username && !this.props.homeApp.user) {
            //GET user data.
        }
        BackHandler.addEventListener('hardwareBackPress', this._onBackPress);

    }
    generateSetupText() {
        if (this.props.homeApp.username) {
            return (
                <Text>Welcome back {this.props.homeApp.username}</Text>
            );
        }
        return (
            <TouchableOpacity onPress={this.openPage.bind(this, 'setup')}><Text>Tap to specify a character to prefill calculators and tools!</Text></TouchableOpacity>
        );
    }

    render() {

        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) =>
                r1 !== r2
        });

        return (

            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", backgroundColor: '#ECECEC' }}>
                <Text>
                    Runescape Toolkit
                </Text>

                {this.generateSetupText()}
                <HomeGrid
                    DataSource={ds}
                    gridData={getGridData(this.openUrl, this.openPage)}
                >
                </HomeGrid>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    selectCharLabel: {
        marginLeft: 20, marginRight: 20
    },
    buttonWhiteText: {
        fontSize: 20,
        color: '#FFF',
    },
    buttonBlackText: {
        fontSize: 15,
        color: '#595856'
    },
    primaryButton: {
        backgroundColor: '#1d1d1d',
        margin: 20
    }, centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
    },
    gray: {
        backgroundColor: '#cccccc',
    },
    headerContainer: {
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
    },
});

export default HomeApp;