const homeImages = {
    logo: {
        uri: 'http://vignette1.wikia.nocookie.net/logopedia/images/5/5b/Runescape-Logo.png'
    },
    maxCape: {
        uri: 'http://vignette4.wikia.nocookie.net/runescape2/images/5/51/Max_cape_detail.png'
    },
    totalLevel: {
        uri: 'https://lh3.ggpht.com/rISLrE2ij7Xi6XdxUBM2MYXyl-hL9S2-8nrpmLqgX0uoJxdw08DUohmgU73rGTD11YE=w300'
    },
    moneyMaking: {
        uri: 'http://vignette4.wikia.nocookie.net/runescape2/images/3/30/Coins_10000.png'
    },
    redditLogo: {
        uri: 'https://image.freepik.com/free-icon/reddit-logo_318-64705.jpg'
    },
    twitchLogo: {
        uri: 'http://seeklogo.com/images/T/twitch-logo-4931D91F85-seeklogo.com.png'
    }
}

let getGridData = (openUrl, openPage) => {
    return [
        {
            rowHeader: true,
            rowHeaderText: 'Tools'
        },
        {
            image: homeImages.totalLevel,
            text: 'Skill Calcs',
            onClick: openPage.bind(this, 'skillcalc')
        },
        {
            image: homeImages.maxCape,
            text: 'Max Calc',
            onClick: openPage.bind(this, 'maxcalc')
        },
        {
            image: homeImages.totalLevel,
            text: 'Efficiency',
            onClick: openPage.bind(this, 'efficiency')
        },
        {
            image: homeImages.moneyMaking,
            text: 'Money making',
            onClick: openPage.bind(this, 'moneymaking')
        },
        {
            rowHeader: true,
            rowHeaderText: 'Useful websites'
        },
        {
            image: homeImages.logo,
            text: 'Forums',
            onClick: openUrl.bind(this, "http://services.runescape.com/m=forum/forums.ws#group63")
        },
        {
            image: homeImages.redditLogo,
            text: '2007 Reddit',
            onClick: openUrl.bind(this, "https://www.reddit.com/r/2007scape/")
        },
        {
            image: homeImages.twitchLogo,
            text: 'Streams',
            onClick: openUrl.bind(this, "https://www.twitch.tv/directory/game/RuneScape")
        }
    ];
};

module.exports = {
    getGridData,
    homeImages
}
