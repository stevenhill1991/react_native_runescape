import React from 'react';

import {
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    Image,
    ListView,
    TouchableHighlight,
    TouchableOpacity
} from 'react-native';

_renderRow = (rowData) => {
    if (rowData.image && rowData.text) {
        return (
            <TouchableOpacity style={styles.item} onPress={rowData.onClick}>
                <View style={styles.center}>
                    <Image resizeMode="contain" source={rowData.image} style={styles.image} />
                    <Text>{rowData.text}</Text>
                </View>
            </TouchableOpacity>
        )
    }
    if (rowData.rowHeader) {
        return (
            <View style={styles.rowHeader}>
                <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                    {rowData.rowHeaderText}
                </Text>
            </View>
        )
    }
}

const HomeGrid = ({ DataSource, gridData }) => {
    return (
        <View>
            <ListView contentContainerStyle={styles.list}
                dataSource={DataSource.cloneWithRows(gridData)}
                renderRow={_renderRow}
            />
        </View>
    );
}

const styles = StyleSheet.create({
    center: {
        alignItems: 'center',
        paddingTop: 10
    },
    list: {
        justifyContent: 'center',
        flexDirection: 'row',
        flexWrap: 'wrap',
        flex: 1,
    },
    itemHeader: {
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#EDEDED',
    },
    item: {
        backgroundColor: '#FFFFFF',
        borderWidth: 1,
        borderColor: '#EDEDED',
        width: 120,
        height: 110,
    },
    image: {
        width: 78,
        height: 58,
        marginTop: 10,
        marginLeft: 15,
        marginRight: 15
    },
    text: {
        color: 'black',
        fontSize: 14,
    },
    rowHeader: {
        alignItems: 'center',
        width: 300,
        height: 25,
        marginTop: 10,
        marginBottom: 10
    },
    textLabel: {
        textAlign: 'center',
        fontSize: 18,
        fontWeight: 'bold',
        fontFamily: 'Verdana',
        marginBottom: 10,
        color: '#595856'
    }
});

export default HomeGrid;