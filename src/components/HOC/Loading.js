import React, { Component } from 'react';
import { ActivityIndicator } from 'react-native';

const Loading = loadingProp => WrappedComponent => {
    return class LoadingHOC extends Component {
        render() {
            return this.props[loadingProp[0]][loadingProp[1]] === true ? 
                (
                    <ActivityIndicator
                        animating={true}
                        size="large"
                    />
                )
                :
                (
                    <WrappedComponent {...this.props} />
                );
    
        }
    };
};

export default Loading;