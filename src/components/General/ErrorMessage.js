import React from 'react';
import {
    StyleSheet,
    Text,
    View,
} from 'react-native';

const ErrorMessage = ({ errorMessage }) => {
    return (
        errorMessage && errorMessage.length ?
            <View style={{
                alignItems: "center",
                justifyContent: "center"
            }}>
                <View style={styles.errorMessagePointer} />
                <View style={styles.errorMessage}>
                    <Text style={styles.errorMessageText}>{errorMessage}</Text>
                </View>
            </View>
            :
            null
    );
}

const styles = StyleSheet.create({
    errorMessage: {
        alignItems: "center",
        justifyContent: "center",
        padding: 5,
        borderColor: 'black',
        marginBottom: 5,
        backgroundColor: 'red'
    },
    errorMessageText: {
        fontSize: 20
    },
    errorMessagePointer: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 5,
        borderRightWidth: 5,
        borderBottomWidth: 10,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        borderBottomColor: 'red'
    }
});

export default ErrorMessage;