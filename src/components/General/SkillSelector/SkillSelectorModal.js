import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions
} from 'react-native';
import _ from 'lodash';

const SKILL_LIST = [
    'Attack',
    'Hitpoints',
    'Mining',
    'Strength',
    'Agility',
    'Smithing',
    'Defence',
    'Herblore',
    'Fishing',
    'Ranged',
    'Thieving',
    'Cooking',
    'Prayer',
    'Crafting',
    'Firemaking',
    'Magic',
    'Fletching',
    'Woodcutting',
    'Runecraft',
    'Slayer',
    'Farming',
    'Construction',
    'Hunter',
    'Overall'
];

getSkillsOrder = () => {
    return _.chunk(SKILL_LIST, 3);
}

getSkillIdx = (skillName, skillData) => {
    return _.findIndex(skillData, ['name', skillName]);
}

getDataRow = (onSubmit, enabledSkillsArr, skill, skillLevel, selectedSkill, key) => {
    return (
        <TouchableOpacity
            key={key}
            onPress={onSubmit.bind(onSubmit, skill)}
            disabled={enabledSkillsArr && enabledSkillsArr.length && !enabledSkillsArr.includes(skill)}
        >
            <View
                style={[
                    enabledSkillsArr.length && enabledSkillsArr.includes(skill) ?
                        styles.nonSelectedSkillImageContainer : styles.disabledSkillImageContainer,
                    selectedSkill === skill.toLowerCase() ? styles.selectedSkill : null
                ]}
                key={key}
            >
                <Image 
                    key={key}
                    style={styles.selectImage}
                    source={{ uri: skill.toLowerCase() }} 
                />
                <Text 
                    style={{ fontSize: 20, color: 'yellow', paddingLeft: 5 }}
                >
                    {skillLevel}
                </Text>
            </View>
        </TouchableOpacity>
    )
}

getSkillRow = (skills, key) => {
    return (
        <View style={{ flexDirection: 'row', padding: 0, elevation: 6 }} key={key}>
            {skills}
        </View>
    );
}

getRows = (onSubmit, onCancel, selectedSkill, userData, enabledSkillsArr) => {
    var self = this;
    return _.map(this.getSkillsOrder(), (row, i) => {
        i = i * 10;
        return getSkillRow(_.map(row, (skill, j) => {
            var skillLevel = "";
            if (userData) {
                skillLevel = skill === "Overall" ? userData.Standard.progressData.level : userData.Standard.skillData[getSkillIdx(skill, userData.Standard.skillData)].level;
            }
            return getDataRow(onSubmit, enabledSkillsArr, skill, skillLevel, selectedSkill, j)
        }), i);;
    });
}

var { height, width } = Dimensions.get('window');

const SkillSelectorModal = ({ onSubmit, onCancel, isVisible, userData, selectedSkill, enabledSkills }) => {
    if (!isVisible) {
        return null;
    }
    return (
        <View style={styles.overlayContainer}>
            <Text style={{ fontSize: 20, color: 'yellow', paddingBottom: 5 }}>
                {userData ? _.startCase(_.toLower(userData.Standard.userName)) : " "}
            </Text>
            {getRows(onSubmit, onCancel, selectedSkill, userData, enabledSkills)}
        </View>
    );
}

var styles = StyleSheet.create({
    overlayContainer: {
        elevation: 5,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        width: width,
        height: height,
        justifyContent: 'flex-start',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
    },
    usernameText: {
        justifyContent: 'center',
        alignItems: 'center',
        color: 'yellow'
    },
    nonSelectedSkillImageContainer: {
        backgroundColor: '#4a4a46',
        width: width / 3 - 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: height / 10,
        borderColor: '#675b4a',
        borderWidth: 2,
        elevation: 7,
        flexDirection: 'row'
    },
    selectedSkill: {
        borderColor: '#e2f442'
    },
    selectImage: {
        width: 30,
        height: 30,
    },
    disabledSkillImageContainer: {
        backgroundColor: 'rgba(120, 32, 32, 0.8)',
        width: width / 3 - 10,
        justifyContent: 'center',
        alignItems: 'center',
        height: height / 10,
        borderColor: '#675b4a',
        borderWidth: 2,
        elevation: 7,
        flexDirection: 'row'
    }
});

export default SkillSelectorModal;