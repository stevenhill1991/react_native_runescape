import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    Image,
    ProgressBar,
    ActivityIndicator
} from 'react-native';
import SetupForm from './SetupForm';
import PlayerFoundModal from './PlayerFoundModal';
import LabelComponent from '../General/Label';
import Loading from '../HOC/Loading';
class SetupApp extends Component {

    onSearchUser = (event, skipSetup) => {
        event.preventDefault();
        if (skipSetup) {
            this.props.setupSubmit(null, null, true);
            return;
        }
        let username = this.props.setupApp.username;
        this.props.searchUser(username);
    }

    onChangeText = (text) => {
        this.props.inputChange(text);
    }

    onPlayerFoundSubmit = () => {
        this.props.setupSubmit(this.props.setupApp.username, this.props.setupApp.user, false);
    }

    onPlayerFoundCancel = () => {
        this.props.closePlayerFoundModal();
    }

    render() {
        // if (this.props.setupApp.isFetching) {
        //     return (
        //         <ActivityIndicator
        //             animating={true}
        //             size="large"
        //         />
        //     );
        // }
        return (
            <View style={styles.mainContainer}>
                <View style={styles.headerContainer}>
                    <LabelComponent text="Runescape Toolkit" />
                </View>
                <SetupForm
                    onSubmit={this.onSearchUser}
                    onChangeText={this.onChangeText}
                    value={this.props.setupApp.username}
                    disabled={this.props.setupApp.username === null ||
                        this.props.setupApp.username === ""}
                    errorMessage={this.props.setupApp.errorMessage}
                />
                <PlayerFoundModal
                    onSubmit={this.onPlayerFoundSubmit}
                    onCancel={this.onPlayerFoundCancel}
                    isVisible={this.props.setupApp.showPlayerFoundModal}
                    userData={this.props.setupApp.user}
                />
            </View>
        )
    }
}
export default Loading(["setupApp","isFetching"])(SetupApp);

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#ECECEC',
        justifyContent: "center",
        alignItems: "center",
    },
    headerContainer: {
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 20,
    },
    primaryButton: {
        backgroundColor: '#303030',
        margin: 20
    },
    buttonWhiteText: {
        fontSize: 20,
        color: '#FFF',
    },
    selectCharLabel: {
        marginLeft: 20,
        marginRight: 20,
        fontSize: 16,
        alignItems: "center",
        justifyContent: "center",
    },
    buttonBlackText: {
        fontSize: 15,
        color: '#595856'
    },
    centering: {
        alignItems: 'center',
        justifyContent: 'center',
        padding: 8,
    },
    gray: {
        backgroundColor: '#cccccc',
    },
});