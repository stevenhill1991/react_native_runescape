import React from 'react';
import {
    StyleSheet,
    Text,
    View,
    Image,
    TouchableOpacity,
    Dimensions,
    Button,
    ScrollView
} from 'react-native';
import { images } from './modalImages';

var { height, width } = Dimensions.get('window');
import _ from 'lodash';
const PlayerFoundModal = ({ onSubmit, onCancel, isVisible, userData }) => {
    if (!onSubmit || !onCancel || !isVisible || !userData) {
        return null;
    }   
   
    return (
        <View style={styles.overlayContainer}>
            <View style={styles.modal}>
                <View style={[styles.center, styles.paddingBottom]}>
                    <Text style={styles.title}>Confirm Account</Text>
                    <Text>{_.capitalize(userData.Standard.userName)}</Text>
                    <Text>
                        <Image
                            source={images.Overall.uri}
                            style={{ width: 50, height: 50 }}
                        />
                        {userData.Standard.progressData.level.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                    </Text>

                    {userData.accountType ?
                        <Text>
                            <Image
                                source={images[userData.accountType].uri}
                                style={{ width: 50, height: 50 }}
                            />
                            {userData.accountType}
                        </Text>
                        :
                        null
                    }
                </View>
                <Button
                    title="Confirm"
                    color='#303030'
                    onPress={onSubmit}
                />
                <View style={[styles.center, styles.paddingBottom]}>
                    <TouchableOpacity onPress={onCancel}>
                        <Text>Cancel</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    );
}

var styles = StyleSheet.create({
    overlayContainer: {
        elevation: 5,
        zIndex: 10,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        width: width,
        height: height,
        padding: 20,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
    },
    modal: {
        padding: 20,
        height: height / 2 - (height / 5),
        width: width - 100,
        backgroundColor: '#ECECEC',
        borderWidth: 5,
        borderColor: '#F4F4F4'
    },
    confirm: {
        color: '#303030',

    },
    center: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    paddingBottom: {
        paddingBottom: 20
    },
    title: {
        paddingBottom: 20, fontWeight: 'bold'
    }

});

export default PlayerFoundModal;