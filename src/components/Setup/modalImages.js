const images = {
    Standard: {
        uri: require('../../images/lookup/Standard.png')
    },
    Ironman: {
        uri: require('../../images/lookup/Ironman.png')
    },
    Ultimate: {
        uri: require('../../images/lookup/Ultimate.png')
    },
    Hardcore: {
        uri: require('../../images/lookup/Hardcore.png')
    },
    Overall: {
        uri: require('../../images/lookup/Overall.png')
    }
}

export {
    images
};