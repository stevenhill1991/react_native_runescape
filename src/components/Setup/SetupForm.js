import React from 'react';
import {
    View,
    Text,
    Button,
    TextInput,
    StyleSheet
} from 'react-native';
import ButtonComponent from '../General/Button';
import LabelComponent from '../General/Label';
import ErrorMessage from '../General/ErrorMessage';
const SetupForm = ({ onSubmit, onChangeText, value, disabled, errorMessage }) => {
    return (
        <View>
            <LabelComponent
                text="Select a main character this will prepopulate tools and calculators."
                styles={{ textLabel: styles.selectCharLabel }}
            />
            <TextInput
                placeholder="Username"
                maxLength={20}
                onSubmitEditing={(event) => {
                    onSubmit.call(onSubmit, event, false)
                }}
                onChangeText={onChangeText}
                value={value}
                style={{ marginLeft: 20, marginRight: 20, fontSize: 18 }}
                enablesReturnKeyAutomatically={true}
            />

            <ErrorMessage errorMessage={errorMessage} />


            <ButtonComponent
                label="Select"
                styles={{ button: styles.primaryButton, label: styles.buttonWhiteText }}
                onPress={(event) => {
                    onSubmit.call(onSubmit, event, false)
                }}
                disabled={!value || value.length < 1 || value.length && value.trim().length < 1}
            />
            <ButtonComponent
                label="Maybe Later"
                styles={{ label: styles.buttonBlackText }}
                onPress={(event) => {
                    onSubmit.call(onSubmit, event, true)
                }}
            />

        </View>
    );
}

const styles = StyleSheet.create({
    selectButtonContainer: {
        margin: 20,
        paddingBottom: 10
    },
    skipButton: {
        fontSize: 15,
        color: '#595856'
    },
    buttonWhiteText: {
        fontSize: 20,
        color: '#FFF',
    },
    buttonBlackText: {
        fontSize: 15,
        color: '#595856'
    },
    primaryButton: {
        backgroundColor: '#303030',
        marginLeft: 20,
        marginRight: 20
    },
    selectCharLabel: {
        marginLeft: 20,
        marginRight: 20,
        fontSize: 16,
        alignItems: "center",
        justifyContent: "center",
    },
});


export default SetupForm;