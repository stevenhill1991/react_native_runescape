import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    BackHandler,
    ScrollView,
    TouchableOpacity
} from 'react-native';

import UserSearchBar from './UserSearchBar';
import ActionsGrid from './ActionsGrid';
import SkillSelectorModal from '../General/SkillSelector/SkillSelectorModal';
import { Actions } from 'react-native-router-flux';
import { getXpForPlayerSkill } from '../../util/player';

class MaxCalcApp extends Component {

    constructor(props) {
        super();
        this._onBackPress = this.onBackPress.bind(this);
    }

    onBackPress() {
        if (this.props.maxCalcApp.isSelectingSkill) {
            this.props.skillCanceled();
            return true;
        }
        BackHandler.remove

        BackHandler.removeEventListener('hardwareBackPress', this._onBackPress);
        return Actions.pop();
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this._onBackPress);
    }

    onSearchUser = (event) => {
        event.preventDefault();
        let username = this.props.maxCalcApp.username;
        this.props.searchUser(username);
    }

    onChangeUserText = (text) => {
        this.props.inputChange(text);
    }

    onStartSearchUser = (event) => {
        event.preventDefault();
        this.props.startSearchUser(event);
    }

    onSkillSelected = (skillName) => {
        this.props.skillSelected(skillName);
    }

    onSkillCanceled = () => {
        this.props.skillCanceled();
    }

    onSelectSkill = () => {
        this.props.selectSkill();
    }

    generateSearchBar() {
        if (this.props.maxCalcApp.isSearching) {
            return (<View><UserSearchBar onSearchUser={this.onSearchUser} onChangeText={this.onChangeUserText} /></View>);
        }
        return (
            <View style={{ flexDirection: 'row', padding: 20 }}>
                <View style={{
                    flex: 0.7, borderRadius: 4,
                    borderWidth: 4,
                    borderColor: 'blue'
                }}>
                    <TouchableOpacity
                        onPress={this.onStartSearchUser.bind(this.onStartSearchUser)
                        }

                    >
                        <Text style={{ fontSize: 25, paddingLeft: 5, paddingBottom: 5 }}>{this.props.maxCalcApp.username}</Text>
                    </TouchableOpacity>

                </View>
                <View >

                    <Button
                        onPress={(event) => {
                            this.onSelectSkill.call(event)
                        }}
                        title="Select Skill"
                    />
                </View>
            </View>
        );
    }

    render() {
        return (
            <View style={{ flex: 1 }}>
                <View style={{ elevation: 1 }}>{this.generateSearchBar()}</View>

                <ScrollView style={{ flex: 0.8 }}>
                    <ActionsGrid
                        isVisible={this.props.maxCalcApp.selectedSkill}
                        skillName={this.props.maxCalcApp.selectedSkill}
                        xpFrom={getXpForPlayerSkill(this.props.maxCalcApp.selectedSkill, this.props.maxCalcApp.user)}
                        xpTo={13000000}
                    />
                </ScrollView>

                <SkillSelectorModal
                    onSubmit={this.onSkillSelected}
                    onCancel={this.onSkillCanceled}
                    isVisible={this.props.maxCalcApp.isSelectingSkill}
                    userData={this.props.maxCalcApp.user}
                    selectedSkill={this.props.maxCalcApp.selectedSkill}
                />
            </View>
        );
    }
}
export default MaxCalcApp;