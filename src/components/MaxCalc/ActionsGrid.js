import React from 'react';
import {
    StyleSheet,
    View,
    ListView,
    Text,
    Dimensions,
    ScrollView
} from 'react-native';
import { Table, TableWrapper, Row, Rows, Col, Cols, Cell } from 'react-native-table-component';

import { getSkillActions } from '../../util/actions';
import _ from 'lodash';
var { height, width } = Dimensions.get('window');
const ActionsGrid = ({ isVisible, skillName, xpFrom, xpTo }) => {
    if (!isVisible || !skillName || !xpTo) {
        return null;
    }
    const tableHead = ['Level', 'Method', 'XP per', '# goal'];
    var skillActions = getSkillActions(xpFrom, xpTo, skillName);
    if (skillActions.error) {
        return (
            <View>
                <Text>{skillActions.error}</Text>
            </View>
        )
    }
    var tableData = _.map(skillActions, (k) => Object.values(k))
    return (
        <View>
            <Table>
                <Row data={tableHead} style={styles.head} textStyle={styles.text} />
                <Rows data={tableData} style={styles.row} textStyle={styles.text} />
            </Table>
        </View>
    );
}

export default ActionsGrid;

const styles = StyleSheet.create({
    head: { height: 40, backgroundColor: '#f1f8ff' },
    text: { padding: 5 },
    row: { height: 100 }
})