import React from 'react';
import {
    Button,
    StyleSheet,
    Text,
    View,
    TextInput,
    ScrollView,
    Image,
    ProgressBar,
    ActivityIndicator,
    ToastAndroid,
    ListView
} from 'react-native';

const UserSearchBar = ({ onSearchUser, onChangeText }) => {
    return (
        <View style={{ flexDirection: 'row', padding: 20 }}>
            <TextInput style={{ flex: 1 }} onChangeText={onChangeText}></TextInput>
            <Button
                onPress={(event) => {
                    onSearchUser.call(onSearchUser, event)
                }}
                title="Search"
            />
        </View>
    );
}
export default UserSearchBar;