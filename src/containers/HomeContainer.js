import { connect } from 'react-redux';
import HomeApp from '../components/Home/HomeApp';

import {
  navToURL,
  navToPage
} from '../store/modules/HomeApp';

function mapStateToProps(state) {
  return {
    homeApp: state.homeApp // gives our component access to state through props.login
  }
}
function mapDispatchToProps(dispatch) {
  return {
    navToURL: (url) => dispatch(navToURL(url)),
    navToPage: (pageName) => dispatch(navToPage(pageName))
  }; // here we're mapping actions to props
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeApp);