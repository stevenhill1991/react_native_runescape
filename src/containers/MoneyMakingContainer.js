import { connect } from 'react-redux';
import MoneyMakingApp from '../components/MoneyMaking/MoneyMakingApp';

import {
    selectGuide,
    selectCategory
} from '../store/modules/MoneyMakingApp';

function mapStateToProps(state) {
  return {
    moneyMakingApp: state.moneyMakingApp // gives our component access to state through props.login
  }
}
function mapDispatchToProps(dispatch) {
  return {
    selectGuide: (guideName) => dispatch(selectGuide(guideName)),
    selectCategory: (catName) => dispatch(selectCategory(catName))    
  }; // here we're mapping actions to props
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MoneyMakingApp);