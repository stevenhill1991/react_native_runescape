import { connect } from 'react-redux';
import SetupApp from '../components/SkillCalc/SkillCalcApp';

import {
  selectSkill,
  selectCategory
} from '../store/modules/SkillCalcApp';

function mapStateToProps(state) {
  return {
    skillCalcApp: state.skillCalcApp // gives our component access to state through props.login
  }
}

function mapDispatchToProps(dispatch) {
  return {
    selectSkill: (skill) => dispatch(selectSkill(skill)),
    selectCategory: (catName) => dispatch(selectCategory(catName))
  }; // here we're mapping actions to props
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SetupApp);