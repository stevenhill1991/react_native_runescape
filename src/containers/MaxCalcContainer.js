import { connect } from 'react-redux';
import MaxCalcApp from '../components/MaxCalc/MaxCalcApp';

import {
  searchUser,
  inputChange,
  startSearchUser,
  skillSelected,
  skillCanceled,
  selectSkill
} from '../store/modules/MaxCalcApp';


function mapStateToProps(state) {
  return {
    maxCalcApp: state.maxCalcApp // gives our component access to state through props.login
  }
}

function mapDispatchToProps(dispatch) {
  return {
    searchUser: (username) => dispatch(searchUser(username)),
    inputChange: (username) => dispatch(inputChange(username)),
    startSearchUser: () => dispatch(startSearchUser()),
    skillSelected: (skillName) => dispatch(skillSelected(skillName)),
    skillCanceled: () => dispatch(skillCanceled()),
    selectSkill: () => dispatch(selectSkill())
  }; // here we're mapping actions to props
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MaxCalcApp);