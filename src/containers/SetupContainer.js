import { connect } from 'react-redux';
import SetupApp from '../components/Setup/SetupApp';

import {
  searchUser,
  setupSubmit,
  inputChange,
  closePlayerFoundModal
} from '../store/modules/SetupApp';

function mapStateToProps(state) {
  return {
    setupApp: state.setupApp // gives our component access to state through props.login
  }
}

function mapDispatchToProps(dispatch) {
  return {
    searchUser: (username) => dispatch(searchUser(username)),
    setupSubmit: (username, user, skipSetup) => dispatch(setupSubmit(username, user, skipSetup)),
    inputChange: (username) => dispatch(inputChange(username)),
    closePlayerFoundModal: () => dispatch(closePlayerFoundModal())
  }; // here we're mapping actions to props
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SetupApp);