import { connect } from 'react-redux';
import EfficiencyApp from '../components/Efficiency/EfficiencyApp';

import {
    selectSkill
} from '../store/modules/EfficiencyApp';

function mapStateToProps(state) {
  return {
    efficiencyApp: state.efficiencyApp // gives our component access to state through props.login
  }
}
function mapDispatchToProps(dispatch) {
  return {
    selectSkill: (skillName) => dispatch(selectSkill(skillName))  
  }; // here we're mapping actions to props
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EfficiencyApp);