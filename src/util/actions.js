
//1-99
let XP_ARRAY = [0, 0, 83, 174, 276, 388, 512, 650, 801, 969, 1154, 1358, 1584, 1833, 2107, 2411, 2746, 3115, 3523, 3973, 4470, 5018, 5624, 6291, 7028, 7842, 8740, 9730, 10824, 12031, 13363, 14833, 16456, 18247, 20224, 22406, 24815, 27473, 30408, 33648, 37224, 41171, 45529, 50339, 55649, 61512, 67983, 75127, 83014, 91721, 101333, 111945, 123660, 136594, 150872, 166636, 184040, 203254, 224466, 247886, 273742, 302288, 333804, 368599, 407015, 449428, 496254, 547953, 605032, 668051, 737627, 814445, 899257, 992895, 1096278, 1210421, 1336443, 1475581, 1629200, 1798808, 1986068, 2192818, 2421087, 2673114, 2951373, 3258594, 3597792, 3972294, 4385776, 4842295, 5346332, 5902831, 6517253, 7195629, 7944614, 8771558, 9684577, 10692629, 11805606, 13034431];
//100-126 + 200 mall
let VIRTUAL_LEVELS = [
    14391160,
    15889109,
    17542976,
    19368992,
    21385073,
    23611006,
    26068632,
    28782069,
    31777943,
    35085654,
    38737661,
    42769801,
    47221641,
    52136869,
    57563718,
    63555443,
    70170840,
    77474828,
    85539082,
    94442737,
    104273167,
    115126838,
    127110260,
    140341028,
    154948977,
    171077457,
    188884740,
    200000000
];

let SKILL_ACTIONS_STORE = {
    Hunter: [{
        name: "Varrock Museum (Miniquest)",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 1000
    }, {
        name: "Copper longtail",
        level: 9,
        xp: getExperienceForLevel(9),
        xpPerAction: 61
    }, {
        name: "Tropical wagtail",
        level: 19,
        xp: getExperienceForLevel(19),
        xpPerAction: 95.8
    }, {
        name: "Swamp lizard",
        level: 29,
        xp: getExperienceForLevel(29),
        xpPerAction: 152
    }, {
        name: "Barb-tailed kebbit",
        level: 33,
        xp: getExperienceForLevel(33),
        xpPerAction: 168
    }, {
        name: "Prickly kebbit",
        level: 37,
        xp: getExperienceForLevel(37),
        xpPerAction: 204
    }, {
        name: "Falconry - Spotted kebbit",
        level: 43,
        xp: getExperienceForLevel(43),
        xpPerAction: 104
    }, {
        name: "Falconry - Dark kebbit",
        level: 57,
        xp: getExperienceForLevel(57),
        xpPerAction: 132
    }, {
        name: "Red salamander",
        level: 59,
        xp: getExperienceForLevel(59),
        xpPerAction: 272
    }, {
        name: "Black salamnder",
        level: 67,
        xp: getExperienceForLevel(67),
        xpPerAction: 319
    }, {
        name: "Black chinchompa",
        level: 73,
        xp: getExperienceForLevel(73),
        xpPerAction: 315
    }],
    Crafting: [{
        name: "Leather gloves",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 13.8
    }, {
        name: "Leather boots",
        level: 7,
        xp: getExperienceForLevel(7),
        xpPerAction: 16.25
    }, {
        name: "Cowl",
        level: 9,
        xp: getExperienceForLevel(9),
        xpPerAction: 18.5
    }, {
        name: "Leather vambraces",
        level: 11,
        xp: getExperienceForLevel(11),
        xpPerAction: 22
    }, {
        name: "Leather body",
        level: 14,
        xp: getExperienceForLevel(14),
        xpPerAction: 25
    }, {
        name: "Leather chaps",
        level: 18,
        xp: getExperienceForLevel(18),
        xpPerAction: 27
    }, {
        name: "Sapphire",
        level: 20,
        xp: getExperienceForLevel(20),
        xpPerAction: 50
    }, {
        name: "Emerald",
        level: 27,
        xp: getExperienceForLevel(27),
        xpPerAction: 67.5
    }, {
        name: "Ruby",
        level: 34,
        xp: getExperienceForLevel(34),
        xpPerAction: 85
    }, {
        name: "Diamond",
        level: 43,
        xp: getExperienceForLevel(43),
        xpPerAction: 107.5
    }, {
        name: "Water battlestaff",
        level: 54,
        xp: getExperienceForLevel(54),
        xpPerAction: 100
    }, {
        name: "Green d'hide body",
        level: 63,
        xp: getExperienceForLevel(63),
        xpPerAction: 186
    }, {
        name: "Black d'hide body",
        level: 84,
        xp: getExperienceForLevel(84),
        xpPerAction: 258
    }],
    Magic: [{
        name: "High Level Alchemy",
        level: 55,
        xp: getExperienceForLevel(55),
        xpPerAction: 65
    }, {
        name: "Magic Imbue",
        level: 78,
        xp: getExperienceForLevel(78),
        xpPerAction: 86
    }],
    Smithing: [{
        name: "Knights Sword (Quest)",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 12725
    }, {
        name: "Iron bar - any items",
        level: 15,
        xp: getExperienceForLevel(15),
        xpPerAction: 25
    }, {
        name: "Steel bar - any items",
        level: 30,
        xp: getExperienceForLevel(30),
        xpPerAction: 37.5
    }, {
        name: "Gold Ore (Goldsmithing gauntlets)",
        level: 40,
        xp: getExperienceForLevel(40),
        xpPerAction: 56.2
    }],
    Cooking: [{
        name: "Jug of wine",
        level: 35,
        xp: getExperienceForLevel(35),
        xpPerAction: 200
    }],
    Prayer: [{
        name: "Dragon bones (Gilded altar)",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 252
    }],
    Firemaking: [{
        name: "Logs",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 40
    }, {
        name: "Oak logs",
        level: 15,
        xp: getExperienceForLevel(15),
        xpPerAction: 60
    }, {
        name: "Willow logs",
        level: 30,
        xp: getExperienceForLevel(30),
        xpPerAction: 90
    }, {
        name: "Maple logs",
        level: 45,
        xp: getExperienceForLevel(45),
        xpPerAction: 135
    }, {
        name: "Yew logs",
        level: 60,
        xp: getExperienceForLevel(60),
        xpPerAction: 202.5
    }, {
        name: "Magic logs",
        level: 75,
        xp: getExperienceForLevel(75),
        xpPerAction: 303.8
    }, {
        name: "Redwood logs",
        level: 90,
        xp: getExperienceForLevel(90),
        xpPerAction: 350
    }],
    Woodcutting: [{
        name: "Logs",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 25
    }, {
        name: "Oak logs",
        level: 15,
        xp: getExperienceForLevel(15),
        xpPerAction: 37.5
    }, {
        name: "Willow logs",
        level: 30,
        xp: getExperienceForLevel(30),
        xpPerAction: 67.5
    }, {
        name: "Teak logs",
        level: 35,
        xp: getExperienceForLevel(35),
        xpPerAction: 85
    }],
    Runecraft: [{
        name: "Abyss Miniquest (Quest)",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 1000
    }, {
        name: "The Eyes of Glouphrie (Quest)",
        level: 9,
        xp: getExperienceForLevel(9),
        xpPerAction: 6000
    }, {
        name: "Fire rune",
        level: 14,
        xp: getExperienceForLevel(14),
        xpPerAction: 7
    }, {
        name: "Nature rune",
        level: 44,
        xp: getExperienceForLevel(44),
        xpPerAction: 9
    }, {
        name: "Law rune",
        level: 54,
        xp: getExperienceForLevel(54),
        xpPerAction: 9.5
    }, {
        name: "Cosmic rune (x2)",
        level: 59,
        xp: getExperienceForLevel(59),
        xpPerAction: 8
    }, {
        name: "Astral rune (x2)",
        level: 82,
        xp: getExperienceForLevel(82),
        xpPerAction: 8.7
    }, {
        name: "Nature rune (x2)",
        level: 91,
        xp: getExperienceForLevel(91),
        xpPerAction: 9
    }, {
        name: "Lava Rune",
        level: 110,
        xp: 40000000,
        xpPerAction: 10.5
    }],
    Thieving: [{
        name: "Men/Women",
        level: 1,
        xp: getExperienceForLevel(1),
        xpPerAction: 8
    }, {
        name: "Cake stall",
        level: 5,
        xp: getExperienceForLevel(5),
        xpPerAction: 16
    }, {
        name: "Silk stall",
        level: 20,
        xp: getExperienceForLevel(20),
        xpPerAction: 24
    }, {
        name: "The Feud (Quest)",
        level: 30,
        xp: getExperienceForLevel(30),
        xpPerAction: 15000
    }, {
        name: "Pyramid plunder (Room 2)",
        level: 37,
        xp: getExperienceForLevel(37),
        xpPerAction: 30
    }, {
        name: "Pyramid plunder (Room 3)",
        level: 41,
        xp: getExperienceForLevel(37),
        xpPerAction: 50
    }, {
        name: "Blackjacking - Bearded bandit",
        level: 45,
        xp: getExperienceForLevel(45),
        xpPerAction: 65
    }, {
        name: "Blackjacking - Bandit",
        level: 55,
        xp: getExperienceForLevel(55),
        xpPerAction: 85
    }, {
        name: "Blackjacking - Menaphite Thug",
        level: 65,
        xp: getExperienceForLevel(65),
        xpPerAction: 137.5
    }, {
        name: "Pyramid plunder (Room 8)",
        level: 91,
        xp: getExperienceForLevel(91),
        xpPerAction: 275
    }]
};

function getExperienceForLevel(level) {
    if (level > 127) {
        return 200000000; //max total xp
    }
    if (level > 99) {
        return VIRTUAL_LEVELS[level - 100];
    }
    return XP_ARRAY[level];
}

function calculateActions(currentXp, goalXp, itemsToLevelWith) {
    if (itemsToLevelWith == null) {
        return {
            error: "No actions found for selected skill"
        }
    }
    if (currentXp > goalXp) {
        return {
            error: "Already at goal!"
        }
    }
    var actions = [];
    itemsToLevelWith.forEach(function (item, i) {
        var highLevelAction = itemsToLevelWith[i + 1] ? currentXp >= itemsToLevelWith[i + 1].xp : currentXp >= goalXp;
        if (highLevelAction) {
            return actions.push(0);
        }
        var neededXp;
        if (itemsToLevelWith[i + 1]) {
            if (goalXp < itemsToLevelWith[i + 1].xp) {
                neededXp = goalXp - currentXp;
            } else {
                neededXp = itemsToLevelWith[i + 1].xp - currentXp;
            }
        } else {
            neededXp = goalXp - currentXp;
        }

        var neededActions = Math.ceil(neededXp / item.xpPerAction);
        actions.push({
            itemLevel: item.level,
            itemName: item.name,
            xpPerAction: item.xpPerAction,
            neededActions: neededActions
        });

        currentXp += (neededActions * item.xpPerAction);
    });
    return actions;
}

function getSkillActions(currentXp, goalXp, skillName) {
    var actions = calculateActions(parseInt(currentXp), parseInt(goalXp), SKILL_ACTIONS_STORE[skillName]);
    if (actions instanceof Array) {
        return actions.filter(function (i) { return i; });
    }
    return actions;
}

export {
    getSkillActions,
    getExperienceForLevel
};