import _ from 'lodash';
let SKILLS_ARRAY = [
  'Overall',
  'Attack',
  'Defence',
  'Strength',
  'Hitpoints',
  'Ranged',
  'Prayer',
  'Magic',
  'Cooking',
  'Woodcutting',
  'Fletching',
  'Fishing',
  'Firemaking',
  'Crafting',
  'Smithing',
  'Mining',
  'Herblore',
  'Agility',
  'Thieving',
  'Slayer',
  'Farming',
  'Runecraft',
  'Hunter',
  'Construction',
];

export function transformHighscoresDataToPlayer(userName, highscoresData) {
  let userData = highscoresData.split(/\n/);
  let skills = SKILLS_ARRAY;
  let skillData = skills.map((skill) => {
    let skillData = userData.shift().split(',');
    return {
      name: skill,
      rank: skillData[0],
      level: skillData[1],
      xp: skillData[2],
    }
  });
  let playerProgress = skillData.shift();
  let player = {
    userName: userName,
    skillData: skillData,
    progressData: playerProgress
  }
  return player;
}

export function getXpForPlayerSkill(skillName, playerData) {
  if (!skillName || !playerData) {
    return null;
  }
  return skillName === "Overall" ? playerData.Standard.progressData.xp : playerData.Standard.skillData[_.findIndex(playerData.Standard.skillData, ['name', skillName])].xp;
}

export function getLvlForPlayerSkill(skillName, playerData) {
  if (!skillName || !playerData) {
    return null;
  }
  return skillName === "Overall" ? playerData.Standard.progressData.level : parseInt(playerData.Standard.skillData[_.findIndex(playerData.Standard.skillData, ['name', skillName])].level);
}