let userData = {
  "userName": "g",
  "skillData": [
    {
      "name": "Attack",
      "rank": "8981",
      "level": "99",
      "xp": "22785820"
    },
    {
      "name": "Defence",
      "rank": "11458",
      "level": "99",
      "xp": "18518030"
    },
    {
      "name": "Strength",
      "rank": "4961",
      "level": "99",
      "xp": "28185761"
    },
    {
      "name": "Hitpoints",
      "rank": "13931",
      "level": "99",
      "xp": "27754357"
    },
    {
      "name": "Ranged",
      "rank": "80896",
      "level": "98",
      "xp": "12503688"
    },
    {
      "name": "Prayer",
      "rank": "5722",
      "level": "99",
      "xp": "13082690"
    },
    {
      "name": "Magic",
      "rank": "29395",
      "level": "99",
      "xp": "13776983"
    },
    {
      "name": "Cooking",
      "rank": "9345",
      "level": "99",
      "xp": "15400325"
    },
    {
      "name": "Woodcutting",
      "rank": "8047",
      "level": "99",
      "xp": "13767796"
    },
    {
      "name": "Fletching",
      "rank": "58780",
      "level": "93",
      "xp": "7430386"
    },
    {
      "name": "Fishing",
      "rank": "9029",
      "level": "99",
      "xp": "13140537"
    },
    {
      "name": "Firemaking",
      "rank": "1049",
      "level": "99",
      "xp": "23278534"
    },
    {
      "name": "Crafting",
      "rank": "22790",
      "level": "87",
      "xp": "4244617"
    },
    {
      "name": "Smithing",
      "rank": "673",
      "level": "99",
      "xp": "17552635"
    },
    {
      "name": "Mining",
      "rank": "10289",
      "level": "92",
      "xp": "6955468"
    },
    {
      "name": "Herblore",
      "rank": "14588",
      "level": "90",
      "xp": "5683497"
    },
    {
      "name": "Agility",
      "rank": "8066",
      "level": "94",
      "xp": "7993953"
    },
    {
      "name": "Thieving",
      "rank": "34010",
      "level": "83",
      "xp": "2703763"
    },
    {
      "name": "Slayer",
      "rank": "45653",
      "level": "92",
      "xp": "6958685"
    },
    {
      "name": "Farming",
      "rank": "30948",
      "level": "88",
      "xp": "4527996"
    },
    {
      "name": "Runecraft",
      "rank": "1453",
      "level": "99",
      "xp": "14264729"
    },
    {
      "name": "Hunter",
      "rank": "29630",
      "level": "84",
      "xp": "3022139"
    },
    {
      "name": "Construction",
      "rank": "27786",
      "level": "83",
      "xp": "2700267"
    }
  ],
  "progressData": {
    "name": "Overall",
    "rank": "6272",
    "level": "2172",
    "xp": "286232656"
  }
};